/*** PRUEBAS COMPONENTE TESTIMONY PANEL ***/
jest.dontMock("../src/App");
jest.dontMock("../src/Body");
jest.dontMock("../src/TestimonyPanel");

import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';

import TestimonyPanel from '../src/TestimonyPanel';
import TestimonyRow from '../src/TestimonyRow';

describe('PANEL TESTIMONIALS', function() {

	var TPanelComponent = ReactTestUtils.renderIntoDocument(<TestimonyPanel/>);

	it("El componente debe estar definido en el BODY", () => {
		expect(ReactTestUtils.isCompositeComponent(TPanelComponent)).toBeTruthy();
	});

	it('El panel debe existir y estar definido como elemento', () => {

	    const panel = ReactTestUtils.scryRenderedDOMComponentsWithClass(TPanelComponent, 'panel-success');
	    expect(panel).toBeTruthy(); 
   });

	it('El titulo del panel debe estar definido', () => {

	    const panel = ReactTestUtils.findRenderedDOMComponentWithClass(TPanelComponent, 'panel-heading');
	    const title = ReactDOM.findDOMNode(panel);
	    expect(title.textContent).toEqual('Testomonials');
   });

	it('Deben estar las tres filas definidas', () => {
	    
		const filas = ReactTestUtils.scryRenderedDOMComponentsWithClass(TPanelComponent, 'row');
	    expect(filas.length === 3).toBeTruthy();
   });

	it('Las imagenes deben estar definidas', () => {
		const imagenes = ReactTestUtils.scryRenderedDOMComponentsWithTag(TPanelComponent, 'img');
		//expect(imagenes).toBeTruthy();
		expect(imagenes.length === 3).toBeTruthy();
	});

	it('Los testimonios deben estar definido', () => {
		const testimonio = ReactTestUtils.scryRenderedDOMComponentsWithTag(TPanelComponent, 'p');
		expect(testimonio).toBeTruthy();
	});

	it('Se debe mostrar el nombre de quien hizo el testimonio', () => {

		const nombre = ReactTestUtils.scryRenderedDOMComponentsWithTag(TPanelComponent, 'strong');
		const nom1 = ReactDOM.findDOMNode(nombre[0]);
		const nom2 = ReactDOM.findDOMNode(nombre[1]);
		const nom3 = ReactDOM.findDOMNode(nombre[2]);
		expect(nom1.textContent).toEqual('Felipe Loyola');
		expect(nom2.textContent).toEqual('Víctor Barría');
		expect(nom3.textContent).toEqual('Daniel Peña');
	});
});