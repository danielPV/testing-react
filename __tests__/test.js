


import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';

import Body from '../src/Body';


/*** PRUEBAS COMPONENTE BODY***/

describe('HolaMundo', function() {

	var BodyComponent = ReactTestUtils.renderIntoDocument(<Body/>);

	it("El componente Hola Mundo debe estar definido", () => {
  		const h1 = ReactTestUtils.findRenderedDOMComponentWithTag(BodyComponent, 'h1');
    	expect(ReactDOM.findDOMNode(h1)).toBeTruthy();
  	});

  	it("El boton sumar esta definido y funciona correctamente", () => {
  		const botonMas = ReactTestUtils.scryRenderedDOMComponentsWithTag(BodyComponent, 'button');
  		const valor = ReactTestUtils.scryRenderedDOMComponentsWithTag(BodyComponent, 'h4');
  		//expect(botonMas[0]).toBeTruthy();
  		ReactTestUtils.Simulate.click(botonMas[0]);
  		expect(valor[1].textContent).toEquals('2');

  	});

});

