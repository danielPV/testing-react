import React from 'react';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import InlineLogin from './InlineLogin';
import NavItem from 'react-bootstrap/lib/NavItem';

export default class HeaderNavigation extends React.Component {
  render() {

    return (
      // aca van los componentes
      <Navbar fixedTop inverse >
        <Navbar.Header>
        <Navbar.Brand>
          <a href="#">Ejemplo Testing</a>
        </Navbar.Brand>
    </Navbar.Header>
  		<Nav>
        <NavItem eventKey={1} href="#">Link</NavItem>
        <NavItem eventKey={2} href="#">Link</NavItem>
  		  
  		</Nav> 
      <Nav pullRight>
        <InlineLogin className='navbar-form' />
      </Nav>
      </Navbar>

    )
  }
}