import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Grid from 'react-bootstrap/lib/Grid';
import Jumbotron from 'react-bootstrap/lib/Jumbotron';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import LearnMore from './LearnMore';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Contador from './Contador';
import Contactos from './Contactos';
import Post from './Post';
import TestimonyPanel from './TestimonyPanel';


export default class Body extends React.Component {

	aumentar(){
		this.setState({
			value: this.state.value + 1
		});
	}

	disminuir(){
		this.setState({
			value: this.state.value - 1
		});
	}

  render() {
    return (
      <div>
        <Grid>
		  <Row>
		    <Col md={4}>
		      {/* Block content */}
		    </Col>

		    <Col md={4}>
		    <br/>
		    <br/>
		    </Col>
		  </Row>
		  <h1>Hola Mundo </h1>
		  <Contador />
		  <br/>
		  <Contactos />
		  <TestimonyPanel />
		</Grid>
      </div>
    );
  }
}