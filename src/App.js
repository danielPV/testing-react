import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-addons-test-utils';
import HeaderNavigation from './HeaderNavigation';
import Body from './Body';
import Footer from './Footer';


ReactDOM.render(
  <div>
    <HeaderNavigation />
    <Body />
    <Footer />
  </div>
  , document.getElementById('app'));