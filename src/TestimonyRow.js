import React, { Component, PropTypes } from 'react';
//import { Meteor } from 'meteor/meteor';
import {Row, Col, Panel, Image} from 'react-bootstrap';

export default class TestimonyRow extends React.Component {
	constructor() {
		super();
  	}

	render() {
	    return (
			<Row>
				<Col md={2}>
					<Image src={"./images/"+this.props.testimony.photo} responsive thumbnail />
				</Col>
				<Col md={10}>
					<strong>{this.props.testimony.name}</strong> from {this.props.testimony.organization}
	              	<p>{this.props.testimony.body}</p>
	              	{/*<blockquote>
					  <p>{this.props.testimony.body}</p>
					  <footer>{this.props.testimony.name} from <cite title="Source Title">{this.props.testimony.organization}</cite></footer>
					</blockquote>*/}
				</Col>
			</Row>
	    );
  	}
}