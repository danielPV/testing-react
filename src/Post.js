import React from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';

var Post = React.createClass({

	getInitialState() {
	  return {show: false};
	},

    showModal() {
      this.setState({show: true});
    },

	hideModal() {
	  this.setState({show: false});
	},



	render: function(){
		return(
			<ButtonToolbar>
		        <Button bsStyle="primary" onClick={this.showModal}>
		          Click Me!
		    </Button>
		    <Modal
	          {...this.props}
	          show={this.state.show}
	          onHide={this.hideModal}
	          dialogClassName="custom-modal"
	        >

	        <Modal.Header closeButton>
			    <Modal.Title id="contained-modal-title-lg">Mensaje</Modal.Title>
			 </Modal.Header>

			 <Modal.Body>
	            <h4>Titulo</h4>
	            <p>Esto es una prueba.</p>
	         </Modal.Body>

	         <Modal.Footer>
	            <Button onClick={this.hideModal}>Cerrar</Button>
	         </Modal.Footer>
	         </Modal>
	         </ButtonToolbar>
			);
	}
});

export default Post; 