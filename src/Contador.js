import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import ReactDOM from 'react-dom';

var Contador = React.createClass({
		getInitialState: function(){
					return {
						value: 1,
					}
				},

		aumentar: function(){
			this.setState({
				value: this.state.value + 1
			});
		},

		disminuir: function(){
			this.setState({
				value: this.state.value - 1
			});
		},

		render: function(){
		    return (
		      
		      <div>
					<h4>Valor: </h4>
					<h4>{this.state.value}</h4>
					<ButtonGroup>
						<Button bsStyle="success" bsSize="small" className="suma" onClick={this.aumentar} >+</Button>
						<Button bsStyle="danger" bsSize="small" className="resta" onClick={this.disminuir} >-</Button>
					</ButtonGroup>
		      </div>

		    );
		  }
});

export default Contador; 