import React, { Component, PropTypes } from 'react';
//import { Meteor } from 'meteor/meteor';
import {Row, Col, Panel} from 'react-bootstrap';

import TestimonyRow from './TestimonyRow';

export default class TestimonyPanel extends React.Component {
	constructor() {
		super();

		this.testomonialsList = [
			{ "_id" : "DsEr75J2ZfA3BJAih", "name" : "Felipe Loyola", "organization" : "Pathkan", "body" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum leo nisi, quis accumsan risus pulvinar quis. In ut dui molestie, sodales urna non, tincidunt dui.", "photo": "profile-photo.png" },
			{ "_id" : "p6d4MTMGCAuT4bZLP", "name" : "Víctor Barría", "organization" : "Pathkan", "body" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum leo nisi, quis accumsan risus pulvinar quis. In ut dui molestie, sodales urna non, tincidunt dui.", "photo": "profile-photo.png" },
			{ "_id" : "vsCMxdHd8t7to7W4v", "name" : "Daniel Peña", "organization" : "Pathkan", "body" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum leo nisi, quis accumsan risus pulvinar quis. In ut dui molestie, sodales urna non, tincidunt dui.", "photo": "profile-photo.png" }
		];
  	}

	render() {
	    return (
			<Panel 
				header="Testomonials" 
				bsStyle="success">
				{this.testomonialsList.map((t) => (
	              <TestimonyRow key={t._id} testimony={t} />
	            ))}
			</Panel>

	    );
  	}
}