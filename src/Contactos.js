import React from 'react';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import FormControl from 'react-bootstrap/lib/FormControl';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';


var Contactos = React.createClass({


	Contacto: function(event){
				    var updatedList = this.state.initialItems;
				    updatedList = updatedList.filter(function(item){
				      return item.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
				    });
				    this.setState({items: updatedList});
				    this.setState({ value: event.target.value });
				},

	componentWillMount: function(){
					this.setState({items: this.state.initialItems})
				},

	getInitialState: function(){
					return {
						initialItems: [
						'Daniel',
						'Jose',
						'Bastian',
						'Luis',
						'Sandra',
						'Hector',
						'Ivan'
						],
						items: [],
						value: ''
					}
				},
	getValidationState() {
	    const length = this.state.items.length;
	    if (length >= 1) return 'success';
	    else if (length < 1) return 'error';
	  },

	render: function(){
		return(
			<div>
				<form>  
				<FormGroup
		          controlId="formBasicText"
		          validationState={this.getValidationState()}
		        >
		         	<FormControl 
		         		type="text" 
		         		placeholder="Buscar" 
		         		value={this.state.value}
		         		onChange={this.Contacto}
		         	/>
		         	<FormControl.Feedback>
				        <Glyphicon glyph="user" />
				     </FormControl.Feedback>
		         	<HelpBlock>La validacion esta sujeta a si existe el contacto.</HelpBlock>
		         </FormGroup>
				</form>
				<ControlLabel>Contactos</ControlLabel>
				<Lista items={this.state.items}/>
			</div>
			)
	}
});

const Lista = React.createClass({
		  render: function(){
		    return (
		      <ul>
		      {
		        this.props.items.map(function(item) {
		          return <li key={item}>{item}</li>
		        })
		       }
		      </ul>
		    )  
		  }
		});

export default Contactos; 